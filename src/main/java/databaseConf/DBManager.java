package databaseConf;

import car.Car;
import customer.Customer;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DBManager {
    private static SessionFactory sessionFactory = null;

    private static void initSessionFactory() {
        Configuration conf = new Configuration();
        conf.configure()
                .addAnnotatedClass(Customer.class)
                .addAnnotatedClass(Car.class);

        sessionFactory = conf.buildSessionFactory();
    }

    static {
        initSessionFactory();
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
