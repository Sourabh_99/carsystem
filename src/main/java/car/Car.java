package car;

import javax.persistence.*;

@Entity
@Table(name = "Car")
public class Car {
    @Id
    @Column(name = "VEHICLENUMBER")
    private String vehicleNumber;

    @Column(name = "TYPEOFCAR")
    private String typeOfCar;

    @Column(name = "FUELCAPACITY")
    private int fuelCapacity;

    @Column(name = "AVAILABLEFUEL")
    private int availableFuel;

    @Column(name = "SERVICESTATUS")
    private String serviceStatus;

    public Car() {

    }

    public Car(String vehicleNumber, String typeOfCar, int fuelCapacity, int availableFuel) {
        this.vehicleNumber = vehicleNumber;
        this.typeOfCar = typeOfCar;
        this.fuelCapacity = fuelCapacity;
        this.availableFuel = availableFuel;
        this.serviceStatus = "INCOMPLETE";
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getTypeOfCar() {
        return typeOfCar;
    }

    public void setTypeOfCar(String typeOfCar) {
        this.typeOfCar = typeOfCar;
    }

    public int getFuelCapacity() {
        return fuelCapacity;
    }

    public void setFuelCapacity(int fuelCapacity) {
        this.fuelCapacity = fuelCapacity;
    }

    public int getAvailableFuel() {
        return availableFuel;
    }

    public void setAvailableFuel(int availableFuel) {
        this.availableFuel = availableFuel;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public int requiredFuel() {
        return (fuelCapacity - availableFuel);
    }
}
