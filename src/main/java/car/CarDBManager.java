package car;

import databaseConf.DBManager;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CarDBManager {
    private static SessionFactory sessionFactory = DBManager.getSessionFactory();

    public CarDBManager() { }

    public void saveCar(Car car) {
        try (Session session = sessionFactory.openSession();) {
            Transaction tx = session.getTransaction();
            tx.begin();

            session.save(car);
            System.out.println("Saved!");

            tx.commit();
        }
    }

    public Car loadCar(String vehicleNumber) {
        try (Session session = sessionFactory.openSession();) {
            Transaction tx = session.getTransaction();
            tx.begin();

            Car car = session.load(Car.class, vehicleNumber);

            tx.commit();

            car = Hibernate.unproxy(car, Car.class);
            return car;
        }
    }

    public void updateStatus(Car car, String status) {
        car.setServiceStatus(status);

        try (Session session = sessionFactory.openSession();) {
            Transaction tx = session.getTransaction();
            tx.begin();

            session.update(car);

            tx.commit();
        }
    }

    public void updateFuelLevel(Car car, int fuel) {
        car.setAvailableFuel(fuel);

        try (Session session = sessionFactory.openSession();) {
            Transaction tx = session.getTransaction();
            tx.begin();

            session.update(car);

            tx.commit();
        }
    }
}
