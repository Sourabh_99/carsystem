package customer;

import car.Car;
import databaseConf.DBManager;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CustomerDBManager {
    private static SessionFactory sessionFactory = DBManager.getSessionFactory();

    public CustomerDBManager() {

    }

    public void saveCustomer(Customer customer) {
        try (Session session = sessionFactory.openSession();) {
            Transaction tx = session.getTransaction();
            tx.begin();

            session.save(customer);
            System.out.println("Saved!");

            tx.commit();
        }
    }

    public Customer loadCustomer(String id) {
        try (Session session = sessionFactory.openSession();) {
            Transaction tx = session.getTransaction();
            tx.begin();
            Customer customer = session.load(Customer.class, id);

            tx.commit();

            customer = Hibernate.unproxy(customer, Customer.class);
            for (var car : customer.getCars()) {
                Hibernate.unproxy(car, Car.class);
            }

            return customer;
        }
    }
}
