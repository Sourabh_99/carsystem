package carServiceCenter;

import car.Car;
import car.CarDBManager;
import carServiceCenter.serviceManager.CarCentralManager;
import carServiceCenter.serviceManager.Status;
import customer.Customer;
import customer.CustomerDBManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CarService {
    private static CustomerDBManager customerDBManager = new CustomerDBManager();
    private static CarCentralManager carCentralManager = new CarCentralManager();
    private static CarDBManager carDBManager = new CarDBManager();

    public static void main(String[] args){
        System.out.println("Welcome to Car Servicing Center\n");

    	// ask customer for information to decide whether to create  a new customer
        // or load a existing customer
        Scanner in = new Scanner(System.in);
        Customer customer;

        System.out.println("Customer Information :");
        System.out.println("Hi, Are you new customer? Y/N");
        String newCustomerAnswer = in.nextLine();
        System.out.println();
        if (newCustomerAnswer.equals("Y")) {
            customer = getCustomerInformation();
            customerDBManager.saveCustomer(customer);
        } else {
            System.out.print("Enter your ID :- ");
            String id = in.nextLine();
            customer = customerDBManager.loadCustomer(id);
        }

        // call serviceCars
        System.out.println();
        System.out.println("Servicing Information :");
        serviceCars(customer.getCars());

        System.out.println("Thank you!");
    }

    public static Customer getCustomerInformation() {
        Scanner in = new Scanner(System.in);

        // get and set customer fields
        Customer customer = new Customer();

        System.out.println("Enter Your information :");

        System.out.print("Enter ID (Should be number) :- ");
        String id = in.nextLine();
        customer.setId(id);

        System.out.print("Enter Name:- ");
        String name = in.nextLine();
        customer.setName(name);

        System.out.println();

        // get cars information
        List<Car> cars = new ArrayList<Car>();
        String vehicleNumber, typeOfCar, answer;
        int fuelCapacity, availableFuel;

        System.out.println("Want to add car information ? Y/N");
        answer = in.nextLine();

        while (answer.equals("Y")) {
            System.out.println("Enter the information :");

            // get vechicleNumber
            System.out.print("Enter vehicle Number :- ");
            vehicleNumber = in.nextLine();

            // get typeOfCar
            System.out.print("Enter type of Car :- ");
            typeOfCar = in.nextLine();

            // get fuelCapacity
            System.out.print("Enter fuel capacity :- ");
            fuelCapacity = in.nextInt();

            // get availableFuel
            System.out.print("Enter available fuel :- ");
            availableFuel = in.nextInt();

            // add car
            cars.add(new Car(vehicleNumber, typeOfCar, fuelCapacity, availableFuel));

            System.out.println();
            System.out.println("Want to add another car ? Y/N");
            in.nextLine();
            answer = in.nextLine();
        }

        customer.setCars(cars);

        return customer;
    }

    public static void serviceCars(List<Car> cars) {
        Scanner in = new Scanner(System.in);

        for (Car car : cars) {
            // service the car if customer wants
            System.out.println("Want to Service the " + car.getTypeOfCar() + ", " + car.getVehicleNumber() + " Car? Y/N");
            String wantToService = in.nextLine();
            if (wantToService.equals("Y")) {
                carDBManager.updateStatus(car, Status.INCOMPLETE.toString());

                String status = carCentralManager.serviceCar(car);
                System.out.println(status);
            }
        }
    }
}
