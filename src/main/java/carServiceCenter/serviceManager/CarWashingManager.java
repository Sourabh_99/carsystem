package carServiceCenter.serviceManager;

import car.Car;
import car.CarDBManager;

public class CarWashingManager {
	private boolean materialAvailableStatus = true;
	
    public Status service(Car car) {
    	Status status = Status.INCOMPLETE;

    	// update status
		CarDBManager carDBManager = new CarDBManager();
		carDBManager.updateStatus(car, status.toString());

    	if (materialAvailableStatus) {
    		System.out.println("Washing the Car...");
    		status = Status.WASHINGCOMPLETE;
    	// material not available
    	} else {
    		status = Status.MATERIALNOTAVAILABLE;
    	}

    	// update status
		carDBManager.updateStatus(car, status.toString());

    	return status;
    }
    
    public boolean getMaterialAvailableStatus() {
    	return materialAvailableStatus;
    }
    
    public void setMaterialAvailableStatus(boolean materialAvailableStatus) {
    	this.materialAvailableStatus = materialAvailableStatus;
    }
}
