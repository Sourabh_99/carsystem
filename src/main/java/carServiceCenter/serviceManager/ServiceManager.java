package carServiceCenter.serviceManager;

import car.Car;

abstract public class ServiceManager {
    abstract public Status service(Car car);
}
