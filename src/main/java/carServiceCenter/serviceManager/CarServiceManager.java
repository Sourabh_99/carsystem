package carServiceCenter.serviceManager;

import car.Car;
import car.CarDBManager;

public class CarServiceManager {
	
    public Status service(Car car) {
		// check status
		Status status = Status.SERVICING;

		// update status
    	CarDBManager carDBManager = new CarDBManager();
    	carDBManager.updateStatus(car, status.toString());

    	// if not sports car, do servicing else return "MATERIALNOTAVAILABLE" status
    	if (!car.getTypeOfCar().equals("Sports Car")) {
    	    System.out.println("Servicing the car...");
    	    status = Status.SERVICINGCOMPLETE;
    	} else {
    		status = Status.MATERIALNOTAVAILABLE;
    	}

    	// update status
		carDBManager.updateStatus(car, status.toString());

    	return status;
    }
    
}
