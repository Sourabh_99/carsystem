package carServiceCenter.serviceManager;

import car.Car;
import car.CarDBManager;

public class CarFuelManager extends ServiceManager {
    private static int availableFuel = 40;
    
    public Status service(Car car) {
    	int capacity = car.requiredFuel();
        Status status = Status.FUELING;

        // update status
        CarDBManager carDBManager = new CarDBManager();
        carDBManager.updateStatus(car, status.toString());

    	// availableFuel = 0
    	// capacity > available fuel
    	if((availableFuel == 0) || (availableFuel < capacity)) {
            status = Status.FUELNOTAVAILABLE;

        // car already full
        } else if(capacity == 0) {
            status = Status.TANKNOTEMPTY;
        } else {
            System.out.println("Fueling car " + capacity + "ltr...");
            availableFuel = availableFuel - capacity;
            status = Status.FUELINGCOMPLETE;
            carDBManager.updateFuelLevel(car, car.getFuelCapacity());
        }

    	// update status
        carDBManager.updateStatus(car, status.toString());

        return status;
    }
    
    public int getAvailableFuel() {
    	return availableFuel;
    }
    
    public void setAvailableFuel(int availableFuel) {
    	CarFuelManager.availableFuel = availableFuel;
    }
}
