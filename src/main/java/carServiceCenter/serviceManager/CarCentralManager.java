package carServiceCenter.serviceManager;

import car.Car;
import car.CarDBManager;

// central manager will assign tasks to appropriate department
public class CarCentralManager {
	private CarServiceManager carServiceManager = new CarServiceManager();
	private CarWashingManager carWashingManager = new CarWashingManager();
    private CarFuelManager carFuelManager = new CarFuelManager();
    
    public String serviceCar(Car car) {
		String[] status = { "Car servicing complete\n",
							"Car fueling complete\n",
							"Car washing complete\n" };
		Status serviceStatus = null;
		CarDBManager carDBManager = new CarDBManager();

		System.out.println("\nNow, Servicing " + car.getVehicleNumber() + "...\n");

		// servicing
		serviceStatus = carServiceManager.service(car);
		if (serviceStatus == Status.MATERIALNOTAVAILABLE) {
			status[0] = "Car servicing incomplete due to unavailablity\n";
		}

		pauseServicing(2000);

		// fueling
		serviceStatus = carFuelManager.service(car);
		if (serviceStatus == Status.FUELNOTAVAILABLE) {
			status[1] = "Car fueling incomplete due to unavailablity\n";
		} else if (serviceStatus == Status.TANKNOTEMPTY) {
			status[1] = "Car fuel tank is already full\n";
		}

		pauseServicing(2000);

		// washing
		serviceStatus = carWashingManager.service(car);
		if (serviceStatus == Status.MATERIALNOTAVAILABLE) {
			status[2] = "Car washing incomplete due to unavailablity\n";
		}

		pauseServicing(2000);

		System.out.println();

		// update status
		carDBManager.updateStatus(car, Status.COMPLETE.toString());

		// prepare message
		String finalStatus = "";
		for (var s : status) {
			finalStatus += s;
		}

		return finalStatus;
	}

	private void pauseServicing(int mlSeconds) {
		try {
			Thread.sleep(mlSeconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
